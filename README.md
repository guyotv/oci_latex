# OCI_latex

Un modèle très simple en latex pour faire des rapports d'informatique. Ce tout
petit modèle est auto-descriptif : en le lisant, vous apprendrez à le faire
fonctionner.

Il est utilisé au lycée comme introduction à LaTeX.